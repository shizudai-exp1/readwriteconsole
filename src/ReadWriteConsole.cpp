//============================================================================
// Name        : ReadWriteConsole.cpp
// Author      : Yasuhiro Noguchi
// Version     : 0.0.1
// Copyright   : GPLv2
// Description : Read/Write via Console
//============================================================================

#include <iostream>
using namespace std;

int main(int argc, char** argv) {
	string message;
	cout << "Please input your message." << endl;
	getline(cin, message);
	cout << "Your Message is " << message << endl;
	return 0;
}
